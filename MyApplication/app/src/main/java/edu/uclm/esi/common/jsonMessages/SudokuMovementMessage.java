package edu.uclm.esi.common.jsonMessages;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Lauri on 04/04/2016.
 */
public class SudokuMovementMessage extends JSONMessage{

    @JSONable
    private int rowCeldaACambiar;
    @JSONable
    private int colCeldaACambiar;
    @JSONable
    private int numeroAPoner;
    @JSONable
    private int idUser;
    @JSONable
    private int idMatch;

    public SudokuMovementMessage(int numeroAPoner, int colCeldaACambiar, int rowCeldaACambiar, int idUser, int idMatch) {
        super(true);
        this.rowCeldaACambiar = rowCeldaACambiar;
        this.colCeldaACambiar = colCeldaACambiar;
        this.numeroAPoner = numeroAPoner;
        this.idUser = idUser;
        this.idMatch = idMatch;
    }

    public SudokuMovementMessage(JSONObject jso) throws JSONException {
        this(Integer.parseInt(jso.get("numeroAPoner").toString()),  Integer.parseInt(jso.get("colCeldaACambiar").toString()), Integer.parseInt(jso.get("rowCeldaACambiar").toString()),Integer.parseInt(jso.get("idUser").toString()),Integer.parseInt(jso.get("idMatch").toString()));
    }

    public int getRowCeldaACambiar() {
        return rowCeldaACambiar;
    }

    public int getColCeldaACambiar() {
        return colCeldaACambiar;
    }

    public int getNumeroAPoner() {
        return numeroAPoner;
    }

    public int getIdUser() {
        return idUser;
    }

    public int getIdMatch() {
        return idMatch;
    }
}


