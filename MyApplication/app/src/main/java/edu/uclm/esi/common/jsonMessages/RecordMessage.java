package edu.uclm.esi.common.jsonMessages;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Usuario on 12/05/2016.
 */
public class RecordMessage extends JSONMessage {

    @JSONable
    private String ranking1;
    @JSONable
    private String ranking2;
    /*@JSONable
    private String usuario1;
    @JSONable
    private String usuario2;
    @JSONable
    private String usuario3;
    @JSONable
    private long tiempo1;
    @JSONable
    private long tiempo2;
    @JSONable
    private long tiempo3;*/

    public RecordMessage(String ranking1, String ranking2) {
        super(true);
        this.ranking1=ranking1;
        this.ranking2=ranking2;


    }

    public RecordMessage(JSONObject jso) throws JSONException {
        this(jso.get("ranking1").toString(), jso.get("ranking2").toString());
    }

    public String getRanking1(){
        return  ranking1;
    }


    public String getRanking2(){
        return  ranking2;
    }

}
