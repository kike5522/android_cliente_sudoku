package com.maco.clientejuegos.gui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.maco.clientejuegos.R;
import com.maco.clientejuegos.domain.Store;
import com.maco.clientejuegos.http.Proxy;

import org.json.JSONException;

import java.util.concurrent.ExecutionException;

import edu.uclm.esi.common.jsonMessages.JSONMessage;
import edu.uclm.esi.common.jsonMessages.RecordMessage;

/**
 * Created by Usuario on 12/05/2016.
 */
public class RankingActivity extends AppCompatActivity {

    private LinearLayout layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_records);
       this.layout = (LinearLayout) findViewById(R.id.layoutRK);


        Store store = Store.get();
        store.setCurrentContext(this);
        try {
            ranking();
        } catch (InterruptedException e) {

            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void ranking() throws InterruptedException, ExecutionException, JSONException {
        RecordMessage msn = new RecordMessage("x", "x");

       // NetTask nt = new NetTask("Records.action", msn);
        Proxy proxy= Proxy.get();
        //JSONMessage jsm = new JSONMessage();
        JSONMessage jsm=proxy.doPost("Records.action", msn);

        if (jsm.getType().equals(RecordMessage.class.getSimpleName())) {
            RecordMessage smam = (RecordMessage) jsm;
            TextView tv = new TextView(this);
            tv.setText( smam.getRanking1());
            tv.setText( smam.getRanking2());
            this.layout.addView(tv);
        }

    }

}