package solution.farandula.enrique_sensores;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by Jesus on 24/04/2017.
 */

public class AccelerometerActivity extends AppCompatActivity implements SensorEventListener{

    private Socket socket;

    private static final int SERVERPORT=5000;
    private static final String SERVER_IP="10.0.2.2";

    private TextView textViewPosX,textViewPosY,textViewPosZ;
    private SensorManager sensorManager;
    private Sensor acelerometro;


    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accelerometer);

        textViewPosX = (TextView)findViewById(R.id.textViewPosX);
        textViewPosY = (TextView)findViewById(R.id.textViewPosY);
        textViewPosZ = (TextView)findViewById(R.id.textViewPosZ);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        acelerometro=sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

    }

    public void onSensorChanged(SensorEvent event) {
            String mensaje;
            float x, y, z;
            x = event.values[0];
            y = event.values[1];
            z = event.values[2];
            textViewPosX.setText("");
            textViewPosY.setText("");
            textViewPosZ.setText("");

            textViewPosX.append(textViewPosX.getText() + "" + x);
            textViewPosY.append(textViewPosY.getText() + "" + y);
            textViewPosZ.append(textViewPosZ.getText() + "" + z);
            mensaje="x:" + x + "y:" + y + "z:" + z;
            new Thread(new ClientThread(mensaje)).start();

        }
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
    protected void onResume(){
        super.onResume();
        sensorManager.registerListener(this, acelerometro, SensorManager.SENSOR_DELAY_NORMAL);
    }
    protected void onPause(){
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    public void accelerometterStop (View view){

        Intent i = new Intent(this, MainActivity.class );

        startActivity(i);
        finish();
    }

    class ClientThread implements Runnable {
        String salida;

        ClientThread (String mensaje){
            salida=mensaje;
        }

        public void run() {

            try{
                InetAddress serverAddr= InetAddress.getByName(SERVER_IP);
                socket = new Socket(serverAddr, SERVERPORT);
                PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
                out.println(salida);

            }catch (UnknownHostException el) {
                el.printStackTrace();
            }catch (IOException el){
                el.printStackTrace();
            }
        }
    }
}
